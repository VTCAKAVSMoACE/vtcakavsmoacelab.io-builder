FROM rust:1.83.0
LABEL maintainer="Addison Crump <me@addisoncrump.info>"

RUN git clone --branch v0.17.1 --depth 1 https://github.com/getzola/zola.git /opt/zola
RUN cargo install --path /opt/zola
RUN cargo install wasm-pack@0.13.1
